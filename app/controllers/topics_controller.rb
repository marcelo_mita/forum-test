class TopicsController < ApplicationController
  def index
    @topics = Topic.find_all_threads
    @new_topic = Topic.new
  end

  def show
    @topic = Topic.find(params[:id])
    @new_topic = Topic.new
  end

  def create
    @topic = Topic.new(topic_params)
    @topic.is_thread = topic_params[:parent_id].empty?
    begin
      @topic.save!
      if topic_params[:parent_id].empty?
        redirect_to topics_path
      else
        @topic = Topic.find(params[:topic][:redirect_id])
        redirect_to @topic
      end
    rescue => e
      puts "Error: #{e}"
    end
  end

  private

  def topic_params
    params.require(:topic).permit(:title, :parent_id)
  end
end
