// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function() {
  $(".reply-btn").click(function() {
    $(".form-wrapper").hide();
    $(this).next(".form-wrapper").show();
  });
});
