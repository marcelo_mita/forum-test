class Topic
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title, type: String
  field :is_thread, type: Boolean
  has_many :replies, class_name: Topic, inverse_of: :parent
  belongs_to :parent, class_name: Topic, inverse_of: :replies, optional: true

  def self.find_all_threads
    return where(is_thread: true)
  end
end
