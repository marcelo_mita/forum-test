# Forum test for R7

This forum is a app test.

Done:
- Posts creation;
- Posts index visualization;
- Post visualization with comments;
- Comments creation;

Missing:
- Forbidden words filter;
- Tests;

The missing part was due to lack of time to work on the test.

* Usage

- Install gems:
`bundle install`

- Up mongo server;

- Up rails server:
`bundle exec rails server`

- Access:
`localhost:3000`
