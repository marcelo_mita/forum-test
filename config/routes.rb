Rails.application.routes.draw do
  root to: 'topics#index'

  get 'topics', to: 'topics#index'
  get 'topics/:id', to: 'topics#show', as: 'topic'

  post 'topics', to: 'topics#create'
end
